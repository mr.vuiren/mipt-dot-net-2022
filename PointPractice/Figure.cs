﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PointPractice
{
    public abstract class Figure
    {
        public abstract int GetDimension();
        public abstract string GetFigureType();
    }
}
