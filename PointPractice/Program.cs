﻿// See https://aka.ms/new-console-template for more information
using PointPractice;

Console.WriteLine("Hello, World!");

var point = new Point("A", 2);
var point2 = new Point("B", 2,2);
var point3 = new Point("C", 2,3,4);

Console.WriteLine(Point.GetNameWithCoordinates(point));
Console.WriteLine(Point.GetNameWithCoordinates(point2));
Console.WriteLine(Point.GetNameWithCoordinates(point3));

point.Paint();
point2.Paint();
point3.Paint();