﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PointPractice
{
    internal class Point : Figure, IPaintable
    {
        public string Name { get; }
       public double X { get; }
       public double Y { get; }
       public double Z { get; }
        public int Dimensions { get; }

        public Point(string name, double x)
        {
            Name = name;
            X = x;
            Dimensions = 1;
        }

        public Point(string name, double x, double y)
        {
            Name = name;
            X = x;
            Y = y;
            Dimensions = 2;
        }

        public Point(string name, double x, double y, double z)
        {
            Name = name;
            X = x;
            Y = y;
            Z = z;
            Dimensions = 3;
        }

        public static string GetNameWithCoordinates(Point point)
        {
            return string.Format("Точка {0}: количество координат: {1}", point.Name, point.Dimensions);
        }

        public override int GetDimension()
        {
            return Dimensions;
        }

        public override string GetFigureType()
        {
            return "Точка";
        }

        public void Paint()
        {
            var resultString = string.Empty;
            switch (Dimensions)
            {
                case 1:
                    resultString = string.Format("Координата точки {0} : {1}", Name, X);
                    break;
                case 2:
                    resultString = string.Format("Координата точки {0} : {1}, {2}", Name, X, Y);
                    break;
                case 3:
                    resultString = string.Format("Координата точки {0} : {1}, {2}, {3}", Name, X, Y, Z);
                    break;
            }

            Console.WriteLine(resultString);
        }
    }
}
