﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PointPractice
{
    public interface IPaintable
    {
        void Paint();
    }
}
